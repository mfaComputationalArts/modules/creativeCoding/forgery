/**
 
  Nathan Adams / With Laser / lxinspc
    Workshops in Creative Coding - Assignment - Week 2 - program a forgery
    A forgery of Dora Maurer's ixek
 
 */


#include "ofApp.h"

bool bShowOriginal = false;

ofImage oOriginalImage;


//Define three main quads as normalised coordinates to the top left of the canvas
//this co-ordinates were got by measuring the canvas using the xScope pointer
//from these we will determine all the intersections needed

//calculations for these normalised co-oridnates are on this spreadsheet https://docs.google.com/spreadsheets/d/1wFkz_O4zIOeZIQr1cLY-o5s9mP76WnNMd0z2s9My7ng/edit?usp=sharing

//Primary quads to draw - these are the three main shapes
ofVec2f orangeQuad[4] = { ofVec2f(0.3415, 0.2117), ofVec2f(0.6229, 0.145), ofVec2f(0.6963, 0.785), ofVec2f(0.3037, 0.7142) };
ofVec2f rustQuad[4] = { ofVec2f(0.4038, 0.2208), ofVec2f(0.6963, 0.1575),ofVec2f(0.7987, 0.7417),ofVec2f(0.4038, 0.6883)};
ofVec2f blueQuad[4] = { ofVec2f(0.614, 0.46), ofVec2f(0.9021, 0.4525), ofVec2f(0.4872, 0.8625), ofVec2f(0.0845, 0.7542)};

//Interpolated quads, these will have calculated positions based on the primary quads
ofVec2f poly2[4];
ofVec2f poly4[4];
ofVec2f poly5[4];
ofVec2f poly7[3];
ofVec2f poly8[4];
ofVec2f poly9[3];
ofVec2f poly10[4];
ofVec2f poly11[4];
ofVec2f poly12[3];

//--------------------------------------------------------------
void ofApp::setup(){
    // set the window size to 1200/1601 which is the same size as our source image
  ofSetWindowShape(899,1200);
  //Load the image of the original painting from bin/data
  oOriginalImage.load("images/dora-maurer-ixek-13-2015.png");

  ofSetBackgroundColor(225, 225, 225);          //Approximation of gallery wall

  //calculate interpolated quads
  
  poly2[0] = rustQuad[0];
  poly2[1] = intersectLines(orangeQuad[1],orangeQuad[2],rustQuad[0],rustQuad[1]);
  poly2[2] = intersectLines(orangeQuad[1],orangeQuad[2],blueQuad[1],blueQuad[2]);
  poly2[3] = intersectLines(rustQuad[3],rustQuad[0],blueQuad[3],blueQuad[0]);
  
  poly4[0] = blueQuad[0];
  poly4[1] = intersectLines(rustQuad[1], rustQuad[2], blueQuad[0], blueQuad[1]);
  poly4[2] = intersectLines(rustQuad[1], rustQuad[2], blueQuad[1], blueQuad[2]);
  poly4[3] = intersectLines(rustQuad[0], rustQuad[3], blueQuad[0], blueQuad[3]);
  
  poly5[0] = intersectLines(blueQuad[0],blueQuad[1],orangeQuad[1],orangeQuad[2]);
  poly5[1] = intersectLines(blueQuad[0],blueQuad[1],rustQuad[1],rustQuad[2]);
  poly5[2] = intersectLines(blueQuad[1],blueQuad[2],rustQuad[1],rustQuad[2]);
  poly5[3] = intersectLines(poly4[2],poly4[3],orangeQuad[1],orangeQuad[2]);

  poly10[0] = intersectLines(orangeQuad[0], orangeQuad[3], blueQuad[0],blueQuad[3]);
  poly10[1] = intersectLines(orangeQuad[1], orangeQuad[2], blueQuad[1],blueQuad[2]);
  poly10[2] = intersectLines(orangeQuad[2], orangeQuad[3], blueQuad[1],blueQuad[2]);
  poly10[3] = orangeQuad[3];
  
  poly11[0] = intersectLines(rustQuad[3],rustQuad[0],poly10[0],poly10[1]);
  poly11[1] = poly10[1];
  poly11[2] = intersectLines(rustQuad[2],rustQuad[3],blueQuad[1],blueQuad[2]);
  poly11[3] = rustQuad[3];
  
  poly8[0] = poly2[3];
  poly8[1] = poly5[3];
  poly8[2] = poly11[1];
  poly8[3] = poly11[0];
  
  poly9[0] = poly8[1];
  poly9[1] = poly5[2];
  poly9[2] = poly8[2];

  poly12[0] = poly11[1];
  poly12[1] = intersectLines(orangeQuad[1], orangeQuad[2], rustQuad[2], rustQuad[3]);
  poly12[2] = poly11[2];
  
  poly7[0] = poly2[3];
  poly7[1] = poly8[3];
  poly7[2] = poly10[0];
}

//--------------------------------------------------------------
void ofApp::update(){

  
}

//--------------------------------------------------------------
void ofApp::draw(){
    
  if (bShowOriginal) {
    //draw the original image - first so it's under everything else
    ofSetColor(255,255,255);      //set to default, white, otherwise we get odd color effects in the image
    oOriginalImage.draw(0,0);
  } else {
    //draw the forgey
    drawPoly(orangeQuad,4,ofColor(233,113,42));
    drawPoly(blueQuad,4,ofColor(23,66,142));
    drawPoly(rustQuad,4,ofColor(131,63,62));

    //intyerpolated quads
    drawPoly(poly2,4,ofColor(164,95,56));
    drawPoly(poly4,4,ofColor(136,129,103));
    drawPoly(poly5,4,ofColor(103,95,120));
    drawPoly(poly10,4,ofColor(41,102,130));
    drawPoly(poly11,4,ofColor(52,121,137));
    drawPoly(poly8,4,ofColor(138,157,102));
    drawPoly(poly9,3,ofColor(57,83,142));
    drawPoly(poly12,3,ofColor(161,88,45));
    drawPoly(poly7,3,ofColor(173,125,61));
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  if (key == 'o') {
    bShowOriginal = !bShowOriginal;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}



void ofApp::drawPoly(ofVec2f points[], int n, ofColor fill) {
  
  ofSetColor(fill);
  ofBeginShape();
  for (int i=0;i<n;i++) {
    ofVertex(points[i].x * ofGetWidth(), points[i].y * ofGetHeight());
  }
  ofEndShape();
  
  
}

ofVec2f ofApp::intersectLines(ofVec2f a, ofVec2f b, ofVec2f c, ofVec2f d) {
  
  //Calculates intersection of line AB with CD
  //based on https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/
  
  double a1 = b.y - a.y;
  double b1 = a.x - b.x;
  double c1 = a1 * (a.x) + b1 * (a.y);
  
//  cout << a1 << ", " << b1 << ", " << c1 << endl;
  
  double a2 = d.y - c.y;
  double b2 = c.x - d.x;
  double c2 = a2 * (c.x) + b2 * (c.y);

//  cout << a2 << ", " << b2 << ", " << c2 << endl;

  
  double dt = a1 * b2 - a2 * b1;
  
//  cout << dt << endl;
  
  if (dt == 0) {
    //Parallel lines which we shouldn't have for this - so return 0,0 - this willbe obvious it is wrong!
    return ofVec2f(0,0);
  } else {
    double x = ((b2 * c1) - (b1 * c2)) / dt;
    double y = ((a1 * c2) - (a2 * c1)) / dt;
//    cout << x << ", " << y << endl;
    return ofVec2f(x, y);
  }
}
